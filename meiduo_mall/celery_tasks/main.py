# Celery入口文件
from celery import Celery
import os

# 在创建celery实例之前,把Django的配置模块家在到环境配置文件变量中
if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'meiduo_mall.settings.dev'

# 创建celery实例
# Celery('别名')
celery_app = Celery('Meiduo')

# 加载配置
# celery_app.config_from_object('配置文件')
celery_app.config_from_object('celery_tasks.config')

# 注册异步任务
celery_app.autodiscover_tasks(['celery_tasks.sms'])