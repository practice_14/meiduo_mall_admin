# '定义任务的文件
# Celery中的所有任务,本只是一个标准python函数
from celery_tasks.sms.yuntongxun.ccp_sms import CCP
from celery_tasks.main import celery_app


@celery_app.task(name='ccp_send_sms_code')
def ccp_send_sms_code(mobile, sms_code):
    """
    发短信的异步任务
    :param mobile:
    :param sms_code:
    :return: 成功0,失败-1
    """
    ret = CCP().send_template_sms(mobile, [sms_code, 4], 1)
    return ret