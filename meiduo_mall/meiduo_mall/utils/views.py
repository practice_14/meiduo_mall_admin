from django.contrib.auth.mixins import LoginRequiredMixin
from django import http


# 自定义LoginRequiredMixin
class LoginRequiredMixinJSONMixin(LoginRequiredMixin):
    """自定义LoginRequiredMixinJSONMixin类,用户未登录,响应JSON,状态码400,"""
    def handle_no_permission(self):
        return http.JsonResponse({'code': 400, 'errmsg': '用户未登录'})
