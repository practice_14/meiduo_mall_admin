# 自定义文件存储类,提供文件下载全路径
from django.core.files.storage import Storage
from django.conf import settings


class FastDFSStorge(Storage):
    """自定义文件存储类"""
    def _open(self, name, mode='rb'):
        """
        打开文件时,自动调用
        :param name: 文件名
        :param mode: 打开模式
        :return:
        """
        pass

    def _save(self, name, content):
        """
        保存文件时自动调用
        :param content:要保存的文件的内容
        :param name:文件名
        :return:
        """
        pass

    def url(self, name):
        """
        返回文件下载全路径
        :param name: 外界image字段传入的文件名:file_id
        :return: 获取文件的完整路径
        """
        # return 'http://image.meiduo.site:8888/' + name
        return settings.FDFS_URL + name