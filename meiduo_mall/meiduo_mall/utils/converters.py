# 自定义路由转换器


class UsernameConverter:
    # 匹配用户名
    regex = '[a-zA-Z0-9_-]{5,20}'

    def to_python(self, value):
        return str(value)

    def to_url(self, value):
        return str(value)


class MobileConverter:
    # 匹配手机号
    regex = '1[3-9]\d{9}'

    def to_python(self, value):
        return int(value)

    def to_url(self, value):
        return int(value)


class AddressesIDConverter:
    """自定义路由转换器匹配收货地址id"""
    regex = '\d+'

    def to_python(self, value):
        return str(value)

    def to_url(self, value):
        return str(value)