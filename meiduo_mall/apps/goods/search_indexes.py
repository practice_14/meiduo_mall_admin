# 创建索引类
# 通过创建索引类，来指明让搜索引擎对哪些字段建立索引，也就是可以通过哪些字段的关键字来检索数据。
# 本项目中对 SKU 信息进行全文检索，所以在 goods 应用中新建 search_indexes.py 文件，用于存放索引类。

from haystack import indexes
from .models import SKU


class SKUIndex(indexes.SearchIndex, indexes.Indexable):
    """SKU索引数据模型类"""
    # 1. - ---》text用来指明搜索引擎对那些字段建立索引
    # 2. - ---》将要建立索引的模型类字段传递至字段text
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        """返回建立索引的模型类"""
        return SKU

    def index_queryset(self, using=None):
        """返回要建立索引的数据查询集"""
        return self.get_model().objects.filter(is_launched=True)