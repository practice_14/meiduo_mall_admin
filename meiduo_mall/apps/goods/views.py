from django.shortcuts import render
from django.views import View
from apps.contents.models import GoodsCategory
from django import http
from apps.goods.models import SKU
from django.core.paginator import Paginator, EmptyPage
from apps.goods.utils import get_breadcrumb
from haystack.views import SearchView

# Create your views here.


class ListView(View):
    """商品列表页"""

    def get(self, request, category_id):
        # 接收参数
        page_num = request.GET.get('page')  # 当前用户想要看的页面
        page_size = request.GET.get('page_size') # 该页中想看的记录个数
        ordering = request.GET.get('ordering')  # 排序字段

        # 校验参数
        # 校验category_id是否存在
        try:
            category = GoodsCategory.objects.get(id=category_id)
        except GoodsCategory.DoesNotExist:
            return http.JsonResponse({'code': 400, 'errmsg': 'category_id不存在'})
        # page_size、page_num、ordering三个参数在排序和分页时校验

        # 实现核心逻辑:排序和分页,面包屑导航
        # 指定分类下,未被下架的sku信息,再排序
        skus = SKU.objects.filter(category=category, is_launched=True).order_by(ordering)
        # 分页查询
        # 创建分页对象:分页其对象=Paginator(要分页的查询集,每页纪录的个数)
        paginator = Paginator(skus, page_size)

        # 获取指定页中的数据:paginator.page(页码)
        try:
            page_skus = paginator.page(page_num)
        except EmptyPage:
            return http.JsonResponse({'code': 400, 'errmsg': '页码错误'})
        # 获取分页后的总页数
        total_pages = paginator.num_pages
        # 注意:捕获异常,防止空页,返回指定异常-->EmptyPage

        # 将分页后的查询集转字典列表
        list = []
        for sku in page_skus:
            list.append({
                "id": sku.id,
                "default_image_url": sku.default_image.url,     # url为自定义文件存储类重写的url的方向
                "name": sku.name,
                "price": sku.price,
            })

        # 面包屑导航
        breadcrumb = get_breadcrumb(category)

        # 响应结果
        return http.JsonResponse({
            'code': 0,
            'errmsg': "OK",
            "breadcrumb": breadcrumb,   # 面包屑导航
            'list': list, # 排序后的分页数据,不能是模型数据,因为JsonResponse不是别模型类
            'count': total_pages,   # 分页后的总页数
        })


class HotGoodsView(View):
    """热销排行"""

    def get(self, request, category_id):
        # 校验参数:校验category_id是否存在
        try:
            category = GoodsCategory.objects.get(id=category_id)
        except GoodsCategory.DoesNotExist:
            return http.JsonResponse({'code': 400, 'errmsg': '参数category_id不存在'})

        # 查询指定分类下,未被下架的销量最好的两款商品-->查询sku
        # 按照销量倒序 order_by('-sales')
        skus = SKU.objects.filter(category=category, is_launched=True).order_by('-sales')[:2]

        # 将查询集转换为字典列表
        hot_skus = []
        for sku in skus:
            hot_skus.append({
                "id": sku.id,
                "default_image_url": sku.default_image.url,     # url为自定义文件存储类重写的url的方向
                "name": sku.name,
                "price": sku.price,
            })

        # 响应数据
        return http.JsonResponse({'code': 0, 'errmsg': 'OK', 'hot_skus': hot_skus})


class MySearchView(SearchView):
    """自定义商品搜索视图"""
    # 目的:重写create_response(),并返回检索后的JSON数据

    def create_response(self):
        """返回检索后的JSON数据"""
        # 获取检索到的数据
        context = self.get_context()

        # 获取检索到的模型数据
        results = context['page'].object_list

        # 遍历result,取出检索到的SKU,再转字典列表
        data_list = []
        for result in results:
            data_list.append({
                "id": result.object.id,
                "name": result.object.name,
                "price": result.object.price,
                "default_image_url": result.object.default_image.url,
                "searchkey": context.get('query'),
                "page_size": context['page'].paginator.num_pages,   # 分页后的总页数
                "count": context['page'].paginator.count,
            })

        # 将检索到的数据转成JSON返回即可
        return http.JsonResponse(data_list, safe=False)