


def get_breadcrumb(cat3):
    """面包屑导航"""
    # 查询二级和一级分类
    cat2 = cat3.parent  # 多方模型类查找一方模型类---->多方模型类对象.一方模型类名小写
    cat1 = cat2.parent

    # 封装面包屑导航名字
    breadcrumb = {
        'cat1': cat1.name,
        'cat2': cat2.name,
        'cat3': cat3.name
    }
    # 注意:这里返回的是分类的名字

    return breadcrumb

