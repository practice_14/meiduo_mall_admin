from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, BadData
from django.conf import settings
from apps.users.models import User

# 定义生成几激活链接
def generate_email_verify_url(user):
    # 创建序列化器对象
    s = Serializer(settings.SECRET_KEY, 3600 * 24)
    # 准备要序列化数据
    data = {'user_id': user.id, 'email': user.email}
    # 序列化
    token = s.dumps(data).decode()
    # 拼接激活链接
    verify_url = settings.EMAIL_VERIFY_URL + token
    # 返回激活链接
    return verify_url


def check_email_verify_url(token):
    """反序列化密文用户信息"""
    # 创建序列化器对象
    s = Serializer(settings.SECRET_KEY,
                   expires_in=60 * 60 * 24)

    try:
        # 准备要序列化数据
        data = s.loads(token)
    except BadData:
        return None
    # 序列化
    else:
        # 提取用户信息
        user_id = data.get('user_id')
        email = data.get('email')
        # 使用user_id和email查询对应的用户对象
        try:
            user = User.objects.get(id=user_id, email=email)
        except User.DoesNotExist:
            return None
        else:
            return user

