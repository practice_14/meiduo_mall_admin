from django.shortcuts import render
from django.views import View
from django import http
from apps.users.models import User, Address
from django.contrib.auth import login,  authenticate, logout
import logging, json, re
from django_redis import get_redis_connection
from meiduo_mall.utils.views import LoginRequiredMixinJSONMixin
from celery_tasks.email.tasks import send_mail_verify_url
from apps.users.utils import check_email_verify_url, generate_email_verify_url
from apps.goods.models import SKU


# Create your views here.

# 日志输出器
logger = logging.getLogger('django')


class UsernameCountView(View):
    """判断用户名是否重复注册"""
    def get(self, request, username):
        """
        查询用户名对应的记录个数
        :param request:
        :param username:用户名
        :return: JSON
        """

        try:
            count = User.objects.filter(username=username).count()
        except BaseException as e:
            logger.error(e)
            return http.JsonResponse({"code": 400, "errmsg": '数据错误'})

        return http.JsonResponse({"code": '0', "errmsg": 'OK', "count": count})


class MobileCountView(View):
    """判断手机号是否重复注册"""

    def get(self, request, mobile):
        """
        查询手机号对应的记录的个数
        :param request:
        :param mobile:手机号
        :return: JSON
        """

        try:
            count = User.objects.filter(mobile=mobile).count()
        except BaseException as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': "数据错误"})
        return http.JsonResponse({'code': '0', 'errmsg': 'OK', 'count': count})


class RegisterView(View):
    """用户注册"""

    def post(self, request):
        """实现注册逻辑"""
        # 接收参数
        json_bytes = request.body
        json_str = json_bytes.decode()
        json_dict = json.loads(json_str)
        # json_dict = json.loads(request.body.decode())
        # json_dict = json.loads(request.body)也可以实现,因为loads做了升级,可以完成解码
        # 提取参数
        username = json_dict.get('username')
        password = json_dict.get('password')
        password2 = json_dict.get('password2')
        mobile = json_dict.get('mobile')
        sms_code_client = json_dict.get('sms_code')
        allow = json_dict.get('allow')

        # 校验参数
        # 判断是否缺少必传参数
        if not all([username, password, password2, allow, mobile, sms_code_client]):
            return http.JsonResponse({"code": 400, 'errmsg': '缺少必传参数'})

        # 判断用户名是否满足格式
        if not re.match(r'^[a-zA-Z_0-9]{5,20}$', username):
            return http.JsonResponse({"code": 400, 'errmsg': '用户名格式错误'})
        # 判断密码格式
        if not re.match(r'^[0-9A-Za-z]{8,20}$', password):
            return http.JsonResponse({"code": 400, 'errmsg': '密码格式错误'})
        # 判断密码2是否与密码相等
        if password2 != password:
            return http.JsonResponse({"code": 400, 'errmsg': '两次输入密码不一致'})
        #  判断手机号是否满足格式要求
        if not re.match(r'^1[3-9]\d{9}$', mobile):
            return http.JsonResponse({'code': 400, 'errmsg': '参数mobile有误'})
        # 判断短信验证码是否正确:(同图形验证码逻辑一样)
        # 建立连接至redis
        redis_conn = get_redis_connection('verify_code')
        # 提取服务端存储的验证码
        sms_code_server = redis_conn.get('sms_%s' % mobile) # 提取到的sms_code是bytes类型
        # 判断短信验证码是否过期
        if not sms_code_server:
            return http.JsonResponse({'code': 400, 'errmsg': '短信验证码失效'})
        # 对比用户输入的短信验证码和服务端存储的是否一致
        if sms_code_client != sms_code_server.decode():
            return http.JsonResponse({"code": 400, 'errmsg': '短信验证码错误'})
        # 判断是否勾选协议
        if allow != True:
            return http.JsonResponse({"code": 400, 'errmsg': '协议未勾选'})

        # 保存注册数据
        try:
            user = User.objects.create_user(username=username, password=password, mobile=mobile)
        except BaseException as e:
            logger.error(e)
            return http.JsonResponse({"code": 400, 'errmsg': '注册失败'})

        # 状态保持
        login(request, user)

        # 注册成功后,将用户名写入cookie,展示至页面
        response = http.JsonResponse({'code': 0, 'errmsg': '注册成功'})
        response.set_cookie('username', user.username, max_age=3600 * 24 * 14)

        # 响应结果
        return response


class LoginView(View):
    """用户登录"""
    def post(self, request):
        # 接收参数
        json_dict = json.loads(request.body.decode())
        account = json_dict.get('username')
        password = json_dict.get('password')
        # remembered值为True或False
        remembered = json_dict.get('remembered')

        # 校验参数
        if not all([account, password]):
            return http.JsonResponse({"code": 400, 'errmsg': '缺少必传参数'})
        # 判断用户名是否满足格式
        # if not re.match(r'^[a-zA-Z_0-9]{5,20}$', username):
        #     return http.JsonResponse({"code": 400, 'errmsg': '用户名格式错误'})
        # 判断密码格式
        if not re.match(r'^[0-9A-Za-z]{8,20}$', password):
            return http.JsonResponse({"code": 400, 'errmsg': '密码格式错误'})

        # 实现多张号登录
        # 判断用户输入的是手机号还是账户名
        if re.match(r'^1[3-9]\d{9}$', account):
            User.USERNAME_FIELD = 'mobile'
        else:
            User.USERNAME_FIELD = 'username'

        # 认证登录用户
        # 使用用户名在用户数据表中查询记录，存在则接着判断密码是否正确
        # 仅证明用户是美多商城登录用户且密码正确,并不能证明用户已登录,验证是否登录需要通过状态保持来判断
        user = authenticate(request=request, username=account, password=password)
        # 判断用户是否认证成功
        if not user:
            return http.JsonResponse({'code': 400, 'errmsg': '用户名或密码错误'})

        # 实现状态保持
        # 根据remembered参数设置状态保持周期
        login(request, user)

        if remembered:
            # 记录登录状态
            request.session.set_expiry(None)    # None表示不传值,Django默认封装保持两周
            # 若记住登录,记住用户名cookie
            # response.set_cookie('username', user.username, max_age=3600 * 24 * 14)
        else:
            # 不纪录登录状态
            request.session.set_expiry(0)
            # 若未记住登录,不保留用户名cookie
            # response.set_cookie('username', user.username)

        # 登录成功后,将用户名写入cookie,展示至页面
        response = http.JsonResponse({'code': 0, 'errmsg': '登录成功'})
        response.set_cookie('username', user.username, max_age=3600 * 24 * 14)

        # 合并购物车
        from apps.carts.utils import merge_cart_cookie_redis
        response = merge_cart_cookie_redis(request, user, response)

        # 响应结果
        return response


class LogoutView(View):
    """推出登录"""
    def delete(self, request):
        """实现退出登录逻辑"""
        # 和登录相反,清除用户状态和清除cookie

        # 清理用户登录状态
        logout(request)

        # 清理用户cookie
        response = http.JsonResponse({'code': 0, 'errmsg': '退出登录'})
        response.delete_cookie('username')

        # 响应结果
        return response


# class UserInfo(View):
#     """用户中心"""
#     def get(self, request):
#         """实现用户中心数据展示"""
#         if not request.user.is_authenticated:
#             # 未登录 - -响应错误信息
#             return http.JsonResponse({'code': 400, 'errmsg': '用户未登录'})
#         else:
#             data_dict = {
#                 'code': 0,
#                 'errmsg': 'OK',
#                 'info_data': {
#                     'username': '',
#                     'mobile': '',
#                     'email': '',
#                     'email_active': '',
#                 },
#             }
#             # 已登录--响应结果
#             return http.JsonResponse(data_dict)


class UserInfo(LoginRequiredMixinJSONMixin, View):
    """用户中心"""
    def get(self, request):
        """实现用户中心数据展示"""
        data_dict = {
            'code': 0,
            'errmsg': 'OK',
            'info_data': {
                'username': request.user.username,
                'mobile': request.user.mobile,
                'email': request.user.email,
                'email_active': request.user.email_active,
            },
        }
        # 已登录--响应结果
        return http.JsonResponse(data_dict)


class EmailView(LoginRequiredMixinJSONMixin, View):
    """添加邮箱"""

    def put(self, request):
        """实现逻辑"""
        # 接收参数
        json_dict = json.loads(request.body.decode())
        email = json_dict.get('email')
        # 校验参数
        if not email:
            return http.JsonResponse({'code': 400, 'errmsg': '缺少email参数'})
        # 校验邮箱格式
        if not re.match(r'^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', email):
            return http.JsonResponse({'code': 400, 'errmsg': '参数email有误'})
        # 实现核心逻辑:同步用户邮箱至数据库
        try:
            request.user.email = email
            request.user.save()
        except BaseException as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '添加邮箱失败'})

        # 发送验证邮件,调用celery异步任务
        verify_url = generate_email_verify_url(user=request.user)
        send_mail_verify_url.delay(email, verify_url)

        # 响应结果
        return http.JsonResponse({'code': 0, 'errmsg': 'OK'})


class EmailActiveView(View):
    """验证邮箱"""
    def put(self, request):
        # 接收参数
        token = request.GET.get('token')
        # 校验参数
        if not token:
            return http.JsonResponse({'code': 400, 'errmsg': '缺少token'})
        # 通过token提取要验证邮箱的用户
        user = check_email_verify_url(token=token)
        # 设置验证邮箱用户的email_active字段为True
        try:
            user.email_active = True
            user.save()
        except BaseException as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '邮箱验证失败'})
        # 响应结果
        return http.JsonResponse({'code': 0, 'errmsg': '邮箱验证成功'})


class CreateAddressView(LoginRequiredMixinJSONMixin, View):
    """新增地址"""
    def post(self, request):
        # 判断用户地址数量(未被逻辑删除)
        count = request.user.addresses.filter(is_deleted=False).count()
        if count >= 20:
            return http.JsonResponse({'code': 400, 'errmsg': '地址数量超过上限'})

        # 接收参数
        json_dict = json.loads(request.body.decode())
        receiver = json_dict.get('receiver')
        province_id = json_dict.get('province_id')
        city_id = json_dict.get('city_id')
        district_id = json_dict.get('district_id')
        place = json_dict.get('place')
        mobile = json_dict.get('mobile')
        tel = json_dict.get('tel')
        email = json_dict.get('email')

        # 校验参数
        if not all([receiver, province_id, city_id, district_id, place, mobile]):
            return http.JsonResponse({'code': 400, 'errmsg': '缺少必传参数'})
        if not re.match(r'^1[3-9]\d{9}$', mobile):
            return http.JsonResponse({'code': 400,
                                      'errmsg': '参数mobile有误'})
        if tel:
            if not re.match(r'^(0[0-9]{2,3}-)?([2-9][0-9]{6,7})+(-[0-9]{1,4})?$', tel):
                return http.JsonResponse({'code': 400,
                                          'errmsg': '参数tel有误'})
        if email:
            if not re.match(r'^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', email):
                return http.JsonResponse({'code': 400,
                                          'errmsg': '参数email有误'})

        # 将用户填写的地址数据保存到数据表
        try:
            address = Address.objects.create(
            user = request.user,
            # user=request.User.id,
            province_id = province_id,
            city_id = city_id,
            district_id = district_id,
            title = receiver,
            receiver = receiver,
            place = place,
            mobile = mobile,
            tel = tel,
            email = email,
            # is_deleted = 有默认值
            )

            # 设置默认地址
            if not request.user.default_address:
                # request.user.default_address = address
                request.user.default_address_id = address.id
                request.user.save()

        except BaseException as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '新增地址失败'})

        # 响应结果:新增地址
        address_dict = {
            "id": address.id,
            "title": address.title,
            "receiver": address.receiver,
            "province": address.province.name,
            "city": address.city.name,
            "district": address.district.name,
            "place": address.place,
            "mobile": address.mobile,
            "tel": address.tel,
            "email": address.email
        }
        return http.JsonResponse({'code': 0, 'errmsg': 'OK', 'address': address_dict,
})


class AddressView(LoginRequiredMixinJSONMixin, View):
    # 展示收货地址
    def get(self, request):
        # 查询当前用户未被逻辑删除的地址
        address_model_list = request.user.addresses.filter(is_deleted=False)

        # 将地址模型列表转成字典列表
        addresses_dict_list = []
        for address in address_model_list:
            address_dict = {
                "id": address.id,
                "title": address.title,
                "receiver": address.receiver,
                "province": address.province.name,
                "city": address.city.name,
                "district": address.district.name,
                "place": address.place,
                "mobile": address.mobile,
                "tel": address.tel,
                "email": address.email
            }

            # 将默认地址移动到最前面
            default_address = request.user.default_address
            if default_address.id == address.id:
                addresses_dict_list.insert(0, address_dict)
            else:
                addresses_dict_list.append(address_dict)

        # 查询当前登录用户默认地址的ID
        default_address_id = request.user.default_address_id

        return http.JsonResponse({'code': 0, 'errmsg': 'ok',
                                  'default_address_id': default_address_id,
                                  "addresses": addresses_dict_list,
                                  'limit': 20,})


class UpdateDestroyAddressView(LoginRequiredMixinJSONMixin, View):
    """修改和删除地址"""
    def put(self, request, address_id):
        """修改地址"""
        # 接收参数:
        # 接收参数
        json_dict = json.loads(request.body.decode())
        receiver = json_dict.get('receiver')
        province_id = json_dict.get('province_id')
        city_id = json_dict.get('city_id')
        district_id = json_dict.get('district_id')
        place = json_dict.get('place')
        mobile = json_dict.get('mobile')
        tel = json_dict.get('tel')
        email = json_dict.get('email')

        # 校验参数
        if not all([receiver, province_id, city_id, district_id, place, mobile]):
            return http.JsonResponse({'code': 400,
                                      'errmsg': '缺少必传参数'})

        if not re.match(r'^1[3-9]\d{9}$', mobile):
            return http.JsonResponse({'code': 400,
                                      'errmsg': '参数mobile有误'})

        if tel:
            if not re.match(r'^(0[0-9]{2,3}-)?([2-9][0-9]{6,7})+(-[0-9]{1,4})?$', tel):
                return http.JsonResponse({'code': 400,
                                          'errmsg': '参数tel有误'})
        if email:
            if not re.match(r'^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', email):
                return http.JsonResponse({'code': 400,
                                          'errmsg': '参数email有误'})
        # 判断地址是否存在,并更新地址信息
        try:
            Address.objects.filter(id=address_id).update(
                user=request.user,
                title=receiver,
                receiver=receiver,
                province_id=province_id,
                city_id=city_id,
                district_id=district_id,
                place=place,
                mobile=mobile,
                tel=tel,
                email=email
            )
        except Exception as e:
            logger.error(e)
            return http.JsonResponse({'code': 400,
                                      'errmsg': '更新地址失败'})

        # 构造响应数据
        address = Address.objects.get(id=address_id)
        address_dict = {
            "id": address.id,
            "title": address.title,
            "receiver": address.receiver,
            "province": address.province.name,
            "city": address.city.name,
            "district": address.district.name,
            "place": address.place,
            "mobile": address.mobile,
            "tel": address.tel,
            "email": address.email
        }

        # 响应更新地址结果
        return http.JsonResponse({'code': 0,
                                  'errmsg': '更新地址成功',
                                  'address': address_dict})

    def delete(self, request, address_id):
        """删除地址"""
        try:
            # 查询要删除的地址
            address = Address.objects.get(id=address_id)
            # 将地址逻辑删除设置为True
            address.is_deleted = True
            address.save()
        except BaseException as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '删除地址失败'})

        # 响应删除地址的结果
        return http.JsonResponse({'code': 0, 'errmsg': '删除地址成功'})


class DefaultAddressView(LoginRequiredMixinJSONMixin, View):
    """设置默认地址"""
    def put(self, request, address_id):
        """设置默认地址"""
        try:
            # 接收参数,查询地址
            address = Address.objects.get(id=address_id)
            # 设置地址为默认地址
            request.user.default_address = address
            request.user.save()
        except BaseException as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '设置默认地址失败'})

        # 响应结果
        return http.JsonResponse({'code': 0, 'errmsg': '设置默认地址成功'})


class UpdateTitleAddressView(View):
    """设置地址标题"""
    def put(self, request, address_id):
        """设置地址标题"""
        # 接收参数:地址标题
        json_dict = json.loads(request.body.decode())
        title = json_dict.get('title')

        try:
            # 查询地址
            address = Address.objects.get(id=address_id)
            # 设置新的地址标题
            address.title = title
            address.save()
        except BaseException as e:
            logger.error(e)
            return http.JsonResponse({'code': 400, 'errmsg': '设置地址标题失败'})

        # 响应结果
        return http.JsonResponse({'code': 0, 'errmsg': '设置地址标题成功'})


class ChangePasswordView(LoginRequiredMixinJSONMixin, View):
    """修改密码"""
    def put(self, request):
        # 接收参数
        dict = json.loads(request.body.decode())
        old_password = dict.get('old_password')
        new_password = dict.get('new_password')
        new_password2 = dict.get('new_password2')

        # 校验参数
        if not all([old_password, new_password, new_password2]):
            return http.JsonResponse({'code': 400,
                                      'errmsg': '缺少必传参数'})

        result = request.user.check_password(old_password)
        if not result:
            return http.JsonResponse({'code': 400,
                                      'errmsg': '原始密码不正确'})

        if not re.match(r'^[0-9A-Za-z]{8,20}$', new_password):
            return http.JsonResponse({'code': 400,
                                      'errmsg': '密码最少8位,最长20位'})

        if new_password != new_password2:
            return http.JsonResponse({'code': 400,
                                      'errmsg': '两次输入密码不一致'})

        # 修改密码
        try:
            request.user.set_password(new_password)
            request.user.save()
        except BaseException as e:
            logger(e)
            return http.JsonResponse({'code': 400, 'errmsg': '修改密码失败'})

        # 清理状态保持信息
        logout(request)

        response = http.JsonResponse({'code': 0, 'errmsg': 'ok'})
        response.delete_cookie('username')

        # 响应结果
        return response


class UserBrowseHistory(LoginRequiredMixinJSONMixin, View):
    """用户浏览记录"""
    def post(self, request):
        """保存用户浏览记录"""
        # 接收参数
        json_dict = json.loads(request.body.decode())
        sku_id = json_dict.get('sku_id')

        # 校验参数:判断sku_id是否存在
        try:
            SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return http.JsonResponse({'code': 400, 'errmsg': '参数sku_id错误'})

        # 实现核心逻辑:操作redis的4号库保存sku_id作为浏览记录        # 创建连接到redis4号库的对象
        user_id = request.user.id
        # 创建连接到redis4号库的对象
        redis_conn = get_redis_connection('history')
        pl = redis_conn.pipeline()
        # 去重
        pl.lrem('history_%s' % user_id, 0, sku_id)
        # 添加
        pl.lpush('history_%s' % user_id, sku_id)
        # 截取
        pl.ltrim('history_%s' % user_id, 0, 4)
        # 执行管道
        pl.execute()

        # 响应结果
        return http.JsonResponse({'code': 0, 'errmsg': 'OK'})

    def get(self, request):
        """查询用户的浏览记录"""
        # 无条件查询redis中的用户浏览记录
        user_id = request.user.id
        # 创建连接至redis4号库
        redis_conn = get_redis_connection('history')
        # 操作redis读取浏览记录
        sku_ids = redis_conn.lrange('history_%s' % user_id, 0, -1)

        # 通过sku_id查询对应的sku
        # sku_model_list = SKU.objects.filter(id__in=sku_ids)
        # 使用filter()搭配in查询时,默认是按照主键id升序排序的

        # 把查询得到的sku查询集转换成字典列表
        skus = []
        for sku_id in sku_ids:
            sku = SKU.objects.get(id=sku_id)
            skus.append({
                "id": sku.id,
                "name": sku.name,
                "default_image_url": sku.default_image.url,
                "price": sku.price,
            })

        return http.JsonResponse({'code': 0, 'errmsg': 'OK', 'skus': skus})